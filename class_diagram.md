# Definisi Class Diagram
   
- Class diagram atau diagram kelas adalah salah satu jenis diagram struktur pada UML yang menggambarkan dengan jelas struktur serta deskripsi class, atribut, metode, dan hubungan dari setiap objek. Ia bersifat statis, dalam artian diagram kelas bukan menjelaskan apa yang terjadi jika kelas-kelasnya berhubungan, melainkan menjelaskan hubungan apa yang terjadi. 

    Diagram kelas ini sesuai jika diimplementasikan ke proyek yang menggunakan konsep object-oriented karena gambaran dari class diagram cukup mudah untuk digunakan.

# Fungsi Class Diagram 
- Agar lebih memahami tentang class diagram, sebaiknya kita mengetahui tentang apa fungsi dari Class diagram itu sendiri dalam sebuah sistem. Jadi, beberapa fungsi class diagram adalah sebagai berikut:

    - Dapat meningkatkan pemahaman mengenai gambaran umum atau suatu skema dari program yang dibuat
    - Dapat menunjukan struktur sebuah sistem dengan sangat jelas
    - Dapat memberikan gambaran tentang perangkat lunak dan relasi-relasi yang ada di dalamnya
    - Dapat menjadi bahan analisis bisnis, serta dapat digunakan untuk model sistem yang akan dibuat dari sisi bisnis

# Komponen penyusun Class Diagram 
- Diagram kelas memiliki tiga komponen penyusun. Berikut ini adalah komponen-komponennya:Contoh komponen class diagram

    ![dok_cl](dok_cl/cl1.jpg)

    - Komponen atas
    Komponen ini berisikan nama class. Setiap class pasti memiliki nama yang berbeda-beda, sebutan lain untuk nama ini adalah simple name (nama sederhana).

    - Komponen tengah
    Komponen ini berisikan atribut dari class, komponen ini digunakan untuk menjelaskan kualitas dari suatu kelas. Atribut ini dapat menjelaskan dapat ditulis lebih detail, dengan cara memasukan tipe nilai.

    - Komponen bawah
    Komponen ini menyertakan operasi yang ditampilkan dalam bentuk daftar. Operasi ini dapat menggambarkan bagaimana suatu class dapat berinteraksi dengan data.

# Hubungan Antar Class Diagram
Berikut adalah hubungan - hubungan yang ada pada diagram class:

1. **Relasi Asosiasi**: Relasi asosiasi menggambarkan hubungan antara dua atau lebih kelas. Contohnya, dalam sebuah sistem perpustakaan, kelas "Pustakawan" dapat memiliki relasi asosiasi dengan kelas "Buku", yang menunjukkan bahwa pustakawan dapat mengelola banyak buku.

2. **Relasi Komposisi**: Relasi komposisi menunjukkan bahwa sebuah kelas terdiri dari bagian-bagian yang merupakan bagian integral dari kelas tersebut. Contohnya, dalam sistem mobil, kelas "Mobil" dapat memiliki relasi komposisi dengan kelas "Roda", karena roda-roda adalah bagian penting dari mobil dan tidak dapat ada tanpa mobil.

3. **Relasi Agregasi**: Relasi agregasi menunjukkan hubungan semantik "sebagai bagian dari". Contohnya, dalam sistem sekolah, kelas "Siswa" dapat memiliki relasi agregasi dengan kelas "Kelas", di mana siswa-siswa adalah bagian dari kelas-kelas tersebut.

4. **Relasi Pewarisan (Inheritance)**: Relasi pewarisan (inheritance) menunjukkan hubungan "is-a" antara kelas-kelas. Contohnya, kelas "Mobil Sport" dapat mewarisi sifat-sifat dan perilaku dari kelas "Mobil", karena mobil sport adalah jenis khusus dari mobil.

5. **Relasi Ketergantungan (Dependency)**: Relasi ketergantungan terjadi ketika sebuah kelas menggunakan atau bergantung pada kelas lain tanpa adanya hubungan langsung. Contohnya, jika kelas "Karyawan" menggunakan kelas "Departemen" untuk mengakses informasi departemen tempat karyawan bekerja, maka ada relasi ketergantungan antara kelas "Karyawan" dan "Departemen".

6. **Relasi Realisasi (Realization)**: Relasi realisasi menunjukkan implementasi sebuah kelas terhadap sebuah interface. Contohnya, jika sebuah kelas "Pegawai" mengimplementasikan interface "Penggajian", maka terdapat relasi realisasi antara kelas "Pegawai" dan "Penggajian".

# Notasi Hubungan Antar Class Diagram
```mermaid
classDiagram
classA --|> classB : Inheritance
classC --* classD : Composition
classE --o classF : Aggregation
classG --> classH : Association
classI -- classJ : Link(Solid)
classK ..> classL : Dependency
classM ..|> classN : Realization
classO .. classP : Link(Dashed)
```

# Notasi Modifier pada Class Diagram
Tanda | Deskripsi
--- | ---
\+ | Modifier public
\- | Modifier private
\# | Modifier protected

# Contoh Class Diagram
- Berikut contoh class diagram yang di buat menggunakan aplikasi, https://app.diagrams.net/ :
![dok_cl](dok_cl/cl2.png)

- Class diagram menggunakan mermaid.js :

```mermaid
    classDiagram
    class Buku {
    +int id_buku
    +char judul_buku
    +char genre_buku
    +int tahun_terbit buku
    +char penulis_buku
    +insert_data()
    +update_data()
    }

    class Buku_asing {
    +int id_buku
    +char judul_buku
    +char genre_buku
    +int tahun_terbit buku
    +char penulis_buku
    +insert_data()
    +update_data()
    }

    class Buku_lokal {
    +int id_buku
    +char judul_buku
    +char genre_buku
    +int tahun_terbit buku
    +char penulis_buku
    +insert_data()
    +update_data()
    }

    class Peminjaman_buku {
    +int id_anggpta
    +int id_petugas
    +int id_buku
    +date tanggal_peminjaman
    +date tanggal_pengembalian
    +insert_data()
    +update_data()
    +pilih_buku()
    }

    class Anggota_perpustakaan {
    + int id_anggota
    + char nama_anggota
    + insert_data()
    + update_data()
    }

    class Petugas_perpustakaan {
    +int id_petugas
    +char nama_petugas
    +insert_data()
    +update_data()
    }

    Buku -- Peminjaman_buku 
    Peminjaman_buku -- Anggota_perpustakaan
    Peminjaman_buku -- Petugas_perpustakaan
    Buku <|-- Buku_asing
    Buku <|-- Buku_lokal
```
# Referensi
- [Memahami Class Diagram Lebih Baik - www.dicoding.com](https://www.dicoding.com/blog/memahami-class-diagram-lebih-baik/)
- [Class Diagram Adalah: Pengertian, Manfaat, Komponen dan Contohnya - https://accurate.id/](https://accurate.id/teknologi/class-diagram-adalah/)
